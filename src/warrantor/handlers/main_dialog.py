import logging

from aiogram import Bot, types


async def cmd_start(message: types.Message):
    logging.info("Got message {}".format(message))
    bot = Bot.get_current()
    return await bot.send_message(message.chat.id, f"Hello, {message.from_user.full_name}!")


async def cmd_help(message: types.Message):
    bot = Bot.get_current()
    return await bot.send_message(message.chat.id, "Got a helpful message for you :)")