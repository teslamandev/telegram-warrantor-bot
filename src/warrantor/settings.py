import pytoml as toml
import pathlib

PROJECT_ROOT = pathlib.Path(__file__).parent.parent.parent


def load_config(path):
    with open(path) as f:
        conf = toml.load(f)
    return conf
