import logging

from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

from .handlers.main_dialog import cmd_help, cmd_start


def register_handlers(dispatcher: Dispatcher):
    dispatcher.register_message_handler(cmd_start, commands=['start', 'welcome'])
    dispatcher.register_message_handler(cmd_help, commands=['help'])


async def on_startup(dispatcher):
    register_handlers(dispatcher)


async def on_shutdown(dispatcher):
    pass


def main(config):
    TELEGRAM_TOKEN = config['telegram']['TOKEN']
    bot = Bot(TELEGRAM_TOKEN)
    dispatcher = Dispatcher(bot)
    executor.start_polling(dispatcher, on_startup=on_startup, on_shutdown=on_shutdown)
