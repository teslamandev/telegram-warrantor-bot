import asyncpgsa


async def init_db(config):
    dsn = construct_db_url(config['database'])
    pool = await asyncpgsa.create_pool(dsn=dsn)
    return pool


def construct_db_url(config):
    dsn = "postgresql://{user}:{password}@{host}:{port}/{database}"
    return dsn.format(
        user=config['DB_USER'],
        password=config['DB_PASS'],
        database=config['DB_NAME'],
        host=config['DB_HOST'],
        port=config['DB_PORT'],
    )
