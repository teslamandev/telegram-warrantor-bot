import enum
from datetime import datetime

from sqlalchemy import (
    Column, Integer, String, Enum,
    DateTime, ForeignKey, CheckConstraint,
)
from sqlalchemy.orm import relationship

from . import Base

__all__ = ("Deal",)


# class DealStatus(enum.Enum):
#     DISCUSSING = 'discussing'
#     UNDERWAY   = 'underway'
#     VERIFYING  = 'verifying'
#     CANCELLED  = 'cancelled'
#     FROZEN     = 'frozen'
#     CLOSED     = 'closed'


class Deal(Base):
    __tablename__ = 'deals'

    id = Column(Integer, primary_key=True)
    client_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    contractor_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    status_id = Column(Integer, ForeignKey('deal_statuses.id'), nullable=False)
    title = Column(String, nullable=True)
    description = Column(String, nullable=True)
    created = Column(DateTime, nullable=False, default=datetime.utcnow)
    updated = Column(DateTime, nullable=False, default=datetime.utcnow)
    finished = Column(DateTime)
    deadline = Column(DateTime)

    client = relationship("User", foreign_keys=[client_id])
    contractor = relationship("User", foreign_keys=[contractor_id])

    __table_args__ = (
        CheckConstraint(client_id != contractor_id),
    )

    def __init__(self, client, contractor, status, title='', description='',
                 created=None, updated=None, finished=None, deadline=None):
        self.client = client
        self.contractor = contractor
        self.status = status
        self.title = title
        self.description = description
        self.created = created or datetime.now()
        self.updated = updated or datetime.now()
        self.finished = finished
        self.deadline = deadline

    def __repr__(self):
        return (f"<Deal #{self.id} '{self.title}'\n\t"
                + f"client:      {self.client.id}\n\t"
                + f"contractor:  {self.contractor.id}\n\t"
                + f"status:      {self.status}\n\t"
                + f"description: {self.description}\n\t"
                + f"created:     {self.created}\n\t"
                + f"updated:     {self.updated}\n\t"
                + f"finished:    {self.finished}\n\t"
                + f"deadline:    {self.deadline}>")
