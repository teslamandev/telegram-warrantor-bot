from datetime import datetime

from sqlalchemy import Column, String, Integer, DateTime

from . import Base

__all__ = ("User",)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=False)
    first_name = Column(String, nullable=False)
    last_name = Column(String)
    username = Column(String, nullable=False, unique=True)
    language = Column(String)
    created = Column(DateTime, nullable=False, default=datetime.utcnow)

    def __init__(self, id, first_name, last_name=None, username=None, language=None, created=None):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.language = language
        self.created = created

    def __repr__(self):
        return (f"<User #{self.id}\n\t"
                + f"first_name:  {self.first_name}\n\t"
                + f"last_name:   {self.last_name}\n\t"
                + f"username:    {self.username}\n\t"
                + f"language:    {self.language}\n\t"
                + f"created:     {self.created}\n")
