from sqlalchemy import Column, Integer, String

from . import Base


class DealStatus(Base):
    __tablename__ = 'deal_statuses'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name

    def __repr__(self):
        return f"<DealStatus {self.name}>"
