from sqlalchemy import Column, ForeignKey, Integer, String

from . import Base


class Credentials(Base):
    __tablename__ = 'credentials'

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    payment_system_id = Column(Integer, ForeignKey('payment_systems.id'), nullable=False)
    account = Column(String, nullable=False)
