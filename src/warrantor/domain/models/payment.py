from datetime import datetime

from sqlalchemy import Column, DateTime, ForeignKey, Integer, CheckConstraint

from . import Base


class Payment(Base):
    __tablename__ = 'payments'

    id = Column(Integer, primary_key=True, autoincrement=True)
    payment_system_id = Column(Integer, ForeignKey('payment_systems.id'), nullable=False)
    amount = Column(Integer, nullable=False)
    currency_id = Column(Integer, ForeignKey('currencies.id'), nullable=False)
    status_id = Column(Integer, ForeignKey('payment_statuses.id'), nullable=False)
    interest = Column(Integer, nullable=False)
    created = Column(DateTime, nullable=False, default=datetime.utcnow)

    __table_args_ = (
        CheckConstraint(amount >= 0),
        CheckConstraint(interest >= 0),
    )

    def __init__(self, id, payment_system_id, amount, currency_id, status_id, interest, created):
        self.id = id
        self.payment_system_id = payment_system_id
        self.amount = amount
        self.currency_id = currency_id
        self.status_id = status_id
        self.interest = interest
        self.created = created

    def __repr__(self):
        return (f"<Payment #{self.id}\n\t"
                + f"payment_system:  {self.payment_system_id}\n\t"
                + f"amount:          {self.amount}\n\t"
                + f"currency:        {self.currency_id}\n\t"
                + f"status:          {self.status_id}\n\t"
                + f"interest:        {self.interest}\n\t"
                + f"created:         {self.created}\n")
