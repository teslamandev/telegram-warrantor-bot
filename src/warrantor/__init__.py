import logging

FORMAT = '%(filename)s - %(levelname)s - [%(asctime)s] - %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
