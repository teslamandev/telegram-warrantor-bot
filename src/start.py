from warrantor.main import main
from warrantor.settings import load_config


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="Provide path to config file")
    args = parser.parse_args()

    if args.config:
        config = load_config(args.config)
        main(config)
    else:
        parser.print_help()
