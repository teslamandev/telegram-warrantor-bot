import pytest

from sqlalchemy.exc import IntegrityError

from src.warrantor.domain.models import User, Deal


def test_deal_users_are_same(tables, db_session):
    user = User(1, "Test", "User", "@testuser", "en")
    deal = Deal(client=user, contractor=user)
    db_session.add(deal)

    with pytest.raises(IntegrityError):
        db_session.commit()
