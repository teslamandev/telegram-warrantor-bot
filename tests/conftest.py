import pytest

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.warrantor.domain.utils import construct_db_url
from src.warrantor.settings import load_config, PROJECT_ROOT
from db_helper import (
    setup_db, teardown_db,
    create_tables, drop_tables
)


@pytest.fixture(scope='session')
def database():
    admin_db_config = load_config(PROJECT_ROOT / 'config' / 'test_admin_config.toml')['database']
    test_db_config = load_config(PROJECT_ROOT / 'config' / 'test_user_config.toml')['database']

    setup_db(executor_config=admin_db_config, target_config=test_db_config)
    yield
    teardown_db(executor_config=admin_db_config, target_config=test_db_config)


@pytest.fixture
def tables(database):
    test_db_config = load_config(PROJECT_ROOT / 'config' / 'test_user_config.toml')['database']

    create_tables(target_config=test_db_config)
    yield
    drop_tables(target_config=test_db_config)


@pytest.fixture(scope='session')
def engine(database):
    test_db_config = load_config(PROJECT_ROOT / 'config' / 'test_user_config.toml')['database']
    db_url = construct_db_url(test_db_config)
    engine = create_engine(db_url)
    yield engine


@pytest.fixture(scope='function')
def db_session(engine):
    Session = sessionmaker(bind=engine)

    session = Session()
    yield session
    session.close()