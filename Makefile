HOST=0.0.0.0
PORT=5000
TEST_PATH=./tests/

.PHONY: run clean test

run:
	python entry.py

clean:
	@rm -rf `find . -name __pycache__`

test: clean
	pytest --verbose --color=yes $(TEST_PATH)

